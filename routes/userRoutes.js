const express = require(`express`);
const res = require('express/lib/response');
const router = express.Router();

// Import units of functions from userController module
const {
    register, 
    getAllUsers,
    checkEmail,
    login,
    profile,
    update
} = require('./../controllers/userControllers')

const {verify, decode} = require('./../auth');
const { update } = require('../models/User');

// router.get('/', (req, res)=>{

//     res.send(`Welcome to get route`)
// })
//GET ALL USERS
router.get('/', async(req, res) => {
    try{
        await getAllUsers().then(result => res.send(result))
    }catch(err){
        res.status(500).json(err)
    }
})
//REGISTER A USER
router.post('/register', async (req, res)=> {
    //console.log(req.body)
    try{
    await register(req.body).then(result => res.send(result))
    } catch(err){
        res.status(500).json(err)
    }
})

//CHECK IF EMAIL ALREADY EXISTS
router.post('/email-exists', async (req, res) =>{
    try{
       await checkEmail(req.body).then(result => res.send(result))

    }catch(error){
        res.status(500).json(error)
    }
})

//LOGIN THE USER
    //authenticates the user
router.post('/login', (req, res) => {
    //console.log(req.body)
    try {
        login(req.body).then(result => res.send(result))
    }catch(err){
        res.status(500).json(err)
    }
})

//RETRIEVE USER INFORMATION
router.get('/profile', verify, async (req, res) => {
        //res.send(`welcome to get request`)
        //console.log(req.headers.authorization)
      const userId = decode(req.headers.authorization)
      //console.log(userId)
        try {
        profile(userId).then(result => res.send(result))
    }catch(err){
        res.status(500).json(err)
    }
})

//Create a route to update user information, make sure the route is secured with token
router.put('/update', verify, async (req, res) => {
    //console.log(decode(req.headers.authorization.id))

    const userId = decode(req.headers.authorization).id
    try{
        await update(userId).then(result => res.send(result))
    }catch(err){
        res.status(500).json(err)
    }

}

***OPTION 1***
router.patch('/isAdmin', verify, async (req, res) => {
	// console.log(req.body)
	// console.log( decode(req.headers.authorization).isAdmin )
	const admin = decode(req.headers.authorization).isAdmin

	try{
		if(admin == true ){
			await adminStatus(req.body).then(result => res.send(result))

		} else {
			res.send(`You are not authorized!`)
		}

	}catch(err){
		res.status(500).json(err)
	}

})


***OPTION 2***
router.patch('/isAdmin', verifyAdmin, async (req, res) => {
	try{
		await adminStatus(req.body).then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}

})


//Export the router module to be used in index.js file
module.exports = router;