const CryptoJS = require("crypto-js");
const { createToken } = require("./../auth");
const User = require('./../models/User')

//REGISTER A USER
module.exports.register = async (reqBody) => {
    //console.log(reqBody)
    const {firstName, lastName, email, password} = reqBody
    
    const newUser = new User({
        firstName:firstName,
        lastName:lastName,
        email:email,
        password: CryptoJS.AES.encrypt(password, process.env.SECRET_PASS).toString()
        //password: bcrypt.hashSync(password, 10)
    })
    return await newUser.save().then(result =>{
        if(result){
            return (result)
        } else {
            if(result == null){
                return false
            }
        }
    })
}
//GET ALL USERS
module.exports.getAllUsers = async () => {

    return await User.find().then(result => result)
}


//CHECK IF EMAIL EXISTS
module.exports.checkEmail = async (reqBody) =>{
        const {email} = reqBody

    return await User.findOne({email: email}).then( (result, err) =>{
        if(result){
            return true
        } else{
            if(result == null){
                return false
            } else {
                return err
            }
        }
    })
}

//LOGIN A USER
module.exports.login = async (reqBody) => {
    
    return await User.findOne({email: reqBody.email}).then((result, err) =>{
        if(result == null){
            return {message: `User does not exist.`}
        } else {
            if (result !== null){
                //check if pw is correct
                const decryptPw = CryptoJS.AES.decrypt(result.password, process.env.SECRET_PASS).toString(CryptoJS.enc.Utf8);

                console.log(reqBody.password == decryptPw)

                if(reqBody.password == decryptPw){
                    //create a token for the user
                    return {token: createToken(result)}
                } else{
                    return {auth: `auth failed`}
                }
            } else {
                return err
            }
        }
    })
}

//RETRIEVE USER INFORMATION
module.exports.profile = async (id) => {

    return await User.findById(id).then(result =>{
        if(result){
            return result
        }else {
            if(result == null){
                return {message: `user does not exist`}
            } else {
                return err
            }
        }
    })
}

//UPDATE USER INFO
module.exports.update = async (userId) => {
    
    return await User.findByIdAndUpdate(userId, {$set: reqBody}, {new:true}).then((result, err) => {
        if(result){
            result.password = "***"
            return result
        } else{
            return err
        }
    }
        
    }


    //CHANGE TO ADMIN STATUS TO TRUE
module.exports.adminStatus = async (reqBody) => {
	const {email} = reqBody

	return await User.findOneAndUpdate({email: email}, {$set: {isAdmin: true}}, {new:true}).then((result, err) => result ? true : err)
}