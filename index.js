const express = require('express');
const mongoose = require('mongoose');
const dotenv = require('dotenv').config();
const cors = require('cors')

const PORT = 3007;
const app = express();


//connect our routes module
const userRoutes = require('./routes/userRoutes');

//Middle ware to handle JSON payloads
app.use(express.json())
app.use(express.urlencoded({extended:true}))
//prevents blocking of requests from client esp from different domains
app.use(cors())

//Connect database to server
mongoose.connect(process.env.MONGO_URL, {useNewUrlParser: true, useUnifiedTopology: true});

//TEST DB CONNECTION
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'))
db.once('open', () => console.log(`Connected to Database`))
  // we're connected!

//Schema
    //schema is in models module

    
//Routes
    //create a middleware to be the root url of all routes
app.use(`/api/users`, userRoutes);        




app.listen(PORT, ()=> console.log(`Sever connected to port ${PORT}`))